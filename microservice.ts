
export class MicroserviceDescriptor {
    name: string
    acceptEvents: string[]
    generatedEventSuffix: string[]
    lambdaPackage: string
    lambdaHandler: string

}
