# How to run

npm install

pulumi stack init `yourstack`

pulumi stack select `yourstack`

packageLambdas.sh (to zip the lambdas needed to target/)

pulumi up

# To get a current value of a sensor
Add the following item to the greenhouse-xxx-event table:
`{
  "Source": "TEMP1",
  "Timestamp": 123,
  "Type": "sensor.temperature.reading-acquired",
  "Value": 23.3
}`

and then run: curl $(pulumi stack output endpointUrl)TEMP1

# To wipe out all the assets
pulumi destroy