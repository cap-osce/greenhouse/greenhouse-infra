import * as aws from "@pulumi/aws";
import * as pulumi from "@pulumi/pulumi";
import {Resource} from "@pulumi/pulumi";
import {lambdaTag} from "./tags";
import {LambdaLanguageType} from "./config";
import {EventSourceMapping, Function, FunctionArgs, Runtime} from "@pulumi/aws/lambda";
import {Table} from "@pulumi/aws/dynamodb";
import {Role} from "@pulumi/aws/iam";


export function createLambda(def: LambdaDef, dependencies?: Resource[]): aws.lambda.Function {

    const tracingConfig = {
        mode: (def.trace === true) ? "Active" : "PassThrough",
    };

    let definition: FunctionArgs = (isS3Package(def.lambdaS3Bucket)) ? {
        runtime: def.runtime,
        environment: def.environment,
        s3Bucket: def.lambdaS3Bucket,
        s3Key: def.lambdaPackage,
        timeout: def.timeout,
        memorySize: def.memorySize,
        handler: def.lambdaHandler,
        role: def.role.arn,
        tracingConfig: tracingConfig
        ,
        tags: lambdaTag(def.prefix, toLambdaLanguageType(def.runtime), def.runtime)
    } : {
        runtime: def.runtime,
        environment: def.environment,
        code: new pulumi.asset.FileArchive(def.lambdaPackage),
        timeout: def.timeout,
        memorySize: def.memorySize,
        handler: def.lambdaHandler,
        role: def.role.arn,
        tracingConfig: tracingConfig
        ,
        tags: lambdaTag(def.prefix, toLambdaLanguageType(def.runtime), def.runtime)
    };

    return new aws.lambda.Function(`${def.name}`, definition,
        {
            dependsOn: joinResources(dependencies, [def.role])
        }
    );
}

export function createEventSourceMapping(def: DynamoEventMappingDef, dependencies?: Resource[]): aws.lambda.EventSourceMapping {
    return new aws.lambda.EventSourceMapping(`${def.name}`, {
            eventSourceArn: def.eventSource.streamArn,
            enabled: true,
            functionName: def.lambda.arn,
            startingPosition: "LATEST",
            batchSize: def.batchSize,
        },
        {
            dependsOn: joinResources([def.eventSource, def.lambda], dependencies)
        });
}

function joinResources(a: Resource[] | undefined, b: Resource[] | undefined): Resource[] {
    let a1 = a || [];
    let b1 = b || [];
    for (const value of b1) {
        a1.push(value);
    }
    return a1;
}

export function createLambdaAndMapper(def: LambdaEventMapperDef, dependencies?: Resource[]): Tuple<Function, EventSourceMapping> {
    const lambdaFunction = createLambda(toLambdaDef(def));
    const eventMapping = createEventSourceMapping(toDynamoEventMappingDef(def, lambdaFunction));
    return new Tuple<Function, EventSourceMapping>(lambdaFunction, eventMapping);
}

function isS3Package(s3Bucket: string | undefined): boolean {
    if (s3Bucket !== undefined && s3Bucket !== "") {
        return true;
    } else {
        return false;
    }
}

export class Tuple<L, R> {
    left: L;
    right: R;

    constructor(l: L, r: R) {
        this.left = l;
        this.right = r;
    }
}

export interface LambdaEventMapperDef {
    readonly prefix: string;
    readonly runtime: Runtime;
    readonly lambdaName: string;
    readonly awsRegion: string;
    readonly environment: {
        variables?: {
            [key: string]: string;
        }
    };
    readonly timeout?: number;
    readonly memorySize?: number;
    readonly lambdaPackage: string;
    readonly lambdaS3Bucket?: string;
    readonly lambdaHandler: string;
    readonly trace?: boolean;
    readonly role: Role;
    readonly eventMapperName: string;
    readonly eventSource: Table;
    readonly batchSize?: number;

}


export function toLambdaDef(def: LambdaEventMapperDef): LambdaDef {
    return {
        prefix: def.prefix,
        runtime: def.runtime,
        name: def.lambdaName,
        awsRegion: def.awsRegion,
        environment: def.environment,
        timeout: def.timeout || 5,
        memorySize: def.memorySize || 128,
        lambdaPackage: def.lambdaPackage,
        lambdaS3Bucket: def.lambdaS3Bucket,
        lambdaHandler: def.lambdaHandler,
        role: def.role,
        trace: def.trace || false,
    };
}

export function toDynamoEventMappingDef(def: LambdaEventMapperDef, lambdaFunction: Function): DynamoEventMappingDef {
    return {
        prefix: def.prefix,
        name: def.eventMapperName,
        eventSource: def.eventSource,
        lambda: lambdaFunction,
        awsRegion: def.awsRegion,
        batchSize: def.batchSize || 5,
    }
}


export interface LambdaDef {
    readonly prefix: string;
    readonly runtime: Runtime;
    readonly name: string;
    readonly awsRegion: string;
    readonly environment: {
        variables?: {
            [key: string]: string;
        }
    };
    readonly timeout?: number;
    readonly memorySize?: number;
    readonly lambdaPackage: string;
    readonly lambdaS3Bucket?: string;
    readonly lambdaHandler: string;
    readonly role: Role;
    readonly trace?: boolean;

}

export interface DynamoEventMappingDef {
    readonly prefix: string;
    readonly name: string;
    readonly eventSource: Table;
    readonly lambda: Function;
    readonly awsRegion?: string;
    readonly batchSize?: number;
}


function toLambdaLanguageType(runtime: Runtime): LambdaLanguageType {
    let type;
    switch (runtime) {
        case "dotnetcore1.0":
            type = LambdaLanguageType.DOTNET;
            break;
        case "dotnetcore2.0":
            type = LambdaLanguageType.DOTNET;
            break;
        case "dotnetcore2.1":
            type = LambdaLanguageType.DOTNET;
            break;
        case "go1.x":
            type = LambdaLanguageType.GO;
            break;
        case "java8":
            type = LambdaLanguageType.JAVA;
            break;
        case "nodejs":
            type = LambdaLanguageType.NODEJS;
            break;
        case "nodejs4.3":
            type = LambdaLanguageType.NODEJS;
            break;
        case "nodejs4.3-edge":
            type = LambdaLanguageType.NODEJS;
            break;
        case "nodejs6.10":
            type = LambdaLanguageType.NODEJS;
            break;
        case "nodejs8.10":
            type = LambdaLanguageType.NODEJS;
            break;
        case "provided":
            type = LambdaLanguageType.NATIVE;
            break;
        case "python2.7":
            type = LambdaLanguageType.PYTHON;
            break;
        case "python3.6":
            type = LambdaLanguageType.PYTHON;
            break;
        default:
            type = LambdaLanguageType.JAVA;
    }
    return type;

}

