const EVENTS = require("./events");
const MEASURE = require("./physical");
const AWS = require("aws-sdk");

const serviceName = process.env.serviceName;
const prefix = process.env.prefix;
const awsRegion = process.env.awsRegion;

const client = new AWS.DynamoDB.DocumentClient({region: `${awsRegion}`});
console.log("EventProcessor for " + serviceName + " up and running!");

exports.handler = function (event, ctx, callback) {
    console.log(serviceName + " EP handling " + event.Records.length + " records");
    for (let i = 0; i < event.Records.length; i++) {
        let value = event.Records[i];
        if (value.eventName == "INSERT"
            && value.dynamodb.NewImage != null) {
            const e = value.dynamodb.NewImage;
            console.log(value.eventName + " event [" + i + "]: " + JSON.stringify(e));
            if (e.Type.S != null && e.Value.N != null) {
                switch (e.Type.S.toString()) {
                    case EVENTS.SensorEvents.SENSOR_TEMPERATURE_READING_ACQUIRED:
                        processTemperatureEvent(callback, e.Value.N.toString());
                        break;
                    case EVENTS.SensorEvents.SENSOR_HUMIDITY_READING_ACQUIRED:
                        processHumidityEvent(callback, e.Value.N.toString());
                        break;
                    case EVENTS.SensorEvents.SENSOR_MOISTURE_READING_ACQUIRED:
                        processMoistureEvent(callback, e.Value.N.toString());
                        break;
                    case EVENTS.SensorEvents.SENSOR_LUMINOSITY_READING_ACQUIRED:
                        processLuminosityEvent(callback, e.Value.N.toString());
                        break;
                    default:
                        console.log("not interested in the event {}", [e.Type.S]);
                }
            }
        } else {
            console.log("Skipping " + value.eventName + " in position " + i);
        }
    }
};

function processTemperatureEvent(callback, value) {
    updatePhysicalMeasureProjection(callback, MEASURE.MEASUREMENT.TEMPERATURE, value);

}

function processHumidityEvent(callback, value) {
    updatePhysicalMeasureProjection(callback, MEASURE.MEASUREMENT.HUMIDITY, value);
}

function processMoistureEvent(callback, value) {
    updatePhysicalMeasureProjection(callback, MEASURE.MEASUREMENT.MOISTURE, value);
    ifTooDryThenSpray(callback, value);
}

function processLuminosityEvent(callback, value) {
    updatePhysicalMeasureProjection(callback, MEASURE.MEASUREMENT.LUMINOSITY, value);
}

function ifTooDryThenSpray(callback, value) {
    if (parseInt(value)<40)
    {
        raiseCommand(callback, "sunflower", "water", 1);
        console.log(`Moisture is too low ${value} sunflower will be sprayed`);
    }
    else
    {
        console.log(`Moisture is acceptable ${value}. Nothing will be actioned`);
    }
}

function updatePhysicalMeasureProjection(callback, type, value) {
    const tableName = `${prefix}-${serviceName}-projection`;
    console.log("Updating projection: " + tableName);
    let params = {
        TableName: `${tableName}`,
        Item: {
            "Type": `${type}`,
            "Value": Number(`${value}`)
        }
    };
    client.put(params, function (err, data) {
        if (err) {
            console.log("failed to update the projection of ", JSON.stringify(err));
            callback(err, null);
        } else {
            console.log(
                "Success: updated the " + type + " projection of " + serviceName + " to: " + value);
            callback(null, data);
        }
    });
    console.log("DynamoDB update requested for " + serviceName + "/" + type + " is " + value);
}

function raiseCommand(callback, source, type, value) {
    const tableName = `${prefix}-commands`;
    //console.log("Updating projection: " + tableName);
    let params = {
        TableName: `${tableName}`,
        Item: {
            "Timestamp": new Date().getTime(),
            "Type": `${type}`,
            "Source": `${source}`,
            "Value": Number(`${value}`)
        }
    };
    client.put(params, function (err, data) {
        if (err) {
            console.log(`Failed to raise command ${type} with value ${value}`, JSON.stringify(err));
            callback(err, null);
        } else {
            console.log(`Success: raised the command ${type} with value ${value}`);
           //callback(null, data);
        }
    });
}

