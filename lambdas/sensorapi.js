const EVENTS = require("./events");
const MEASURE = require("./physical");
const AWS = require("aws-sdk");

const prefix = process.env.prefix;
const awsRegion = process.env.awsRegion;
const tableName = `${prefix}-sensor-projection`;

const client = new AWS.DynamoDB.DocumentClient({ region: `${awsRegion}` });
console.log("API lambda up and running!");

exports.handler =  function(event, ctx, callback) {
    
    const sensorId = event.pathParameters["sensorId"];
    console.log(`Get sensor ${sensorId} value`);

    const params = {
        TableName: tableName,
        Key: {
            Source: `${sensorId}`
        },
        ConsistentRead: true
    };

    client.get(params, function (err, data) {
        if (err) {
            console.log("failed to get the sensor projection of ", JSON.stringify(err));
            callback(err, null);
        } else {
            if (data.Item != null) {
                const value = data.Item.Value;
                const type = data.Item.Type;
                const timestamp = data.Item.Timestamp;
                //callback(null, data);
                console.log(`Sensor ${sensorId} value is: ${value}`);
                const response = {
                    "statusCode": 200,
                    "headers": {
                    },
                    "body": JSON.stringify({ sensorId, type, timestamp, value }),
                    "isBase64Encoded": false
                };
                callback(null, response);
            }else{
                callback(null, {
                    "statusCode": 404,
                    "headers": {
                    },
                    "body": "{}",
                    "isBase64Encoded": false
                });
            }
        }
    });
};