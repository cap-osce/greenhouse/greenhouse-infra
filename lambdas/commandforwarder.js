const AWS = require("aws-sdk");

const awsRegion = process.env.awsRegion;
var iotdata = new AWS.IotData({endpoint:`a37ijjxxqs59t7-ats.iot.${awsRegion}.amazonaws.com`});

exports.handler = function (event, ctx, callback) {
    console.log("Command lambda handling " + event.Records.length + " records");
    for (let i = 0; i < event.Records.length; i++) {
        let value = event.Records[i];
        if (value.eventName == "INSERT"
            && value.dynamodb.NewImage != null) {
            const e = value.dynamodb.NewImage;
            
            console.log(value.eventName + " event [" + i + "]: " + JSON.stringify(e));
            if (e.Type.S != null && e.Value.N != null) {
                
                var payload = {
                    name: 'light',
                    value: e.Value.N
                };

                var params = {
                    topic: `command/${e.Type.S}`,
                    payload: JSON.stringify(payload),
                    qos: 0
                };

                iotdata.publish(params, function(err, data){
                    if(err) {
                        console.log(err);
                    } else {
                        console.log(`Successfully sent command to ${params.topic}`);
                    }
                });
            }
        } else {
            console.log("Skipping " + value.eventName + " in position " + i);
        }
    }
};

