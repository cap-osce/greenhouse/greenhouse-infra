const AWS = require("aws-sdk");

const SensorEvents = {
    SENSOR_TEMPERATURE_READING_ACQUIRED: "sensor.temperature.reading-acquired",
    SENSOR_HUMIDITY_READING_ACQUIRED: "sensor.humidity.reading-acquired",
    SENSOR_MOISTURE_READING_ACQUIRED: "sensor.moisture.reading-acquired",
    SENSOR_LUMINOSITY_READING_ACQUIRED: "sensor.luminosity.reading-acquired",
}

const prefix = process.env.prefix;
const awsRegion = process.env.awsRegion;
const client = new AWS.DynamoDB.DocumentClient({region: `${awsRegion}`});
const tableName = `${prefix}-events`;

exports.handler = function (event, ctx) {

    console.log(event);
    
    var value = event.data.data;
    var timestamp = parseInt(event.metadata.timestamp);
    var sourceId = event.metadata.sourceId;
    var type = event.metadata.payloadType;
    var typeName;
    if (type === 'temperature') {
        typeName = SensorEvents.SENSOR_TEMPERATURE_READING_ACQUIRED;        
    } else if (type === 'light') {
        typeName = SensorEvents.SENSOR_LUMINOSITY_READING_ACQUIRED;
    } else if (type === 'moisture') {
        typeName = SensorEvents.SENSOR_MOISTURE_READING_ACQUIRED;
    } else if (type === 'humidity') {
        typeName = SensorEvents.SENSOR_HUMIDITY_READING_ACQUIRED;
    } else {
        typeName = "UNKNOWN";
    }

    if (!timestamp) {
        timestamp = new Date().getTime();
    }

    let params = {
        TableName: `${tableName}`,
        Item: {
            "Timestamp": timestamp,
            "Type": typeName,
            "Source": `${sourceId}`,
            "Value": parseFloat(`${event.data.data}`)
        }
    };

    client.put(params, function (err, data) {
        if (err) {
            console.log("failed to update the projection of ", JSON.stringify(err));
        } else {
            console.log("Success: data sent to DynamoDB");
        }
    });

}