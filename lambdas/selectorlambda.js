const EVENTS = require("./events");
const MEASURE = require("./physical");
const AWS = require("aws-sdk");

const serviceName = process.env.serviceName;
const prefix = process.env.prefix;
const awsRegion = process.env.awsRegion;

const client = new AWS.DynamoDB.DocumentClient({region: `${awsRegion}`});
console.log("EventProcessor for " + serviceName + " up and running!");

exports.handler = function (event, ctx, callback) {

};
