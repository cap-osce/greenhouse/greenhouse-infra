const EVENTS = require("./events");
const MEASURE = require("./physical");
const AWS = require("aws-sdk");

const prefix = process.env.prefix;
const awsRegion = process.env.awsRegion;

const client = new AWS.DynamoDB.DocumentClient({region: `${awsRegion}`});
console.log("Projection lambda up and running!");

exports.handler = function (event, ctx, callback) {
    console.log("Projection lambda handling " + event.Records.length + " records");
    for (let i = 0; i < event.Records.length; i++) {
        let value = event.Records[i];
        if (value.eventName == "INSERT"
            && value.dynamodb.NewImage != null) {
            const e = value.dynamodb.NewImage;
            
            console.log(value.eventName + " event [" + i + "]: " + JSON.stringify(e));
            if (e.Type.S != null && e.Value.N != null) {
                switch (e.Type.S.toString()) {
                    case EVENTS.SensorEvents.SENSOR_TEMPERATURE_READING_ACQUIRED:
                        updateTemperature(callback, e.Source.S.toString(), e.Timestamp.N.toString(), e.Value.N.toString());
                        break;
                    case EVENTS.SensorEvents.SENSOR_HUMIDITY_READING_ACQUIRED:
                        updateHumidity(callback, e.Source.S.toString(), e.Timestamp.N.toString(), e.Value.N.toString());
                        break;
                    case EVENTS.SensorEvents.SENSOR_MOISTURE_READING_ACQUIRED:
                        updateMoisture(callback, e.Source.S.toString(), e.Timestamp.N.toString(), e.Value.N.toString());
                        break;
                    case EVENTS.SensorEvents.SENSOR_LUMINOSITY_READING_ACQUIRED:
                        updateLuminosity(callback, e.Source.S.toString(), e.Timestamp.N.toString(), e.Value.N.toString());
                        break;
                    default:
                        console.log("not interested in the event {}", [e.Type.S]);
                }
            }
        } else {
            console.log("Skipping " + value.eventName + " in position " + i);
        }
    }


};

function updateTemperature(callback, source, timestamp, value) {
    updatePhysicalMeasureProjection(callback, source, MEASURE.MEASUREMENT.TEMPERATURE, timestamp, value);
}

function updateHumidity(callback, source, timestamp, value) {
    updatePhysicalMeasureProjection(callback, source, MEASURE.MEASUREMENT.HUMIDITY, timestamp, value);
}

function updateMoisture(callback, source, timestamp, value) {
    updatePhysicalMeasureProjection(callback, source, MEASURE.MEASUREMENT.MOISTURE, timestamp, value);
}

function updateLuminosity(callback, source, timestamp, value) {
    updatePhysicalMeasureProjection(callback, source, MEASURE.MEASUREMENT.LUMINOSITY, timestamp, value);
}

function updatePhysicalMeasureProjection(callback, source, type, timestamp, value) {
    const tableName = `${prefix}-sensor-projection`;
    console.log("Updating projection: " + tableName);
    let params = {
        TableName: `${tableName}`,
        Item: {
            "Source": `${source}`,
            "Type": `${type}`,
            "Timestamp": Number(`${timestamp}`),
            "Value": Number(`${value}`)
        }
    };
    client.put(params, function (err, data) {
        if (err) {
            console.log("failed to update the sensor projection of ", JSON.stringify(err));
            callback(err, null);
        } else {
            console.log(
                "Success: updated the " + type + " of " + source + " to: " + value);
            callback(null, data);
        }
    });
    console.log("DynamoDB update requested for " + source + "/" + type + " is " + value);
}
