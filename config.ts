import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import { tags } from "./tags";

const config = new pulumi.Config("greenhouse-infra");

export enum LambdaLanguageType {
    DOTNET = "dotnet",
    PYTHON = "python",
    GO = "go",
    NODEJS = "nodejs",
    JAVA = "java",
    NATIVE = "native"
}

class EventDescriptor {
    topic: string
    source: string
    eventType: string
}

export class GreenhouseConfig {

    readonly values: EventDescriptor[] = [
        {
            topic: "temperature/1",
            source: "cactus/1",
            eventType: "TemperatureChanged"
        },
        {
            topic: "moisture/1",
            source: "cactus/1",
            eventType: "MoistureChanged"
        },
        {
            topic: "light/3",
            source: "cactus/1",
            eventType: "LightChanged"
        }
    ]

    run(prefix: String) {
        this.values.forEach(mapping => {
            const topic = mapping.topic
            const source = mapping.source
            const event = mapping.eventType

            const paramSource = new aws.ssm.Parameter(`/${prefix}/sensor/${topic}/source`, {
                name: `/${prefix}/sensor/${topic}/source`,
                description: `Source for ${topic}`,
                type: "String",
                value: source,
                overwrite: true,
                tags: tags(prefix)
            })

            const paramTopic = new aws.ssm.Parameter(`/${prefix}/sensor/${topic}/eventType`, {
                name: `/${prefix}/sensor/${topic}/eventType`,
                description: `Event type for ${topic}`,
                type: "String",
                value: event,
                overwrite: true,
                tags: tags(prefix)
            })
        })
    }
}