import * as aws from "@pulumi/aws";
import {LambdaLanguageType} from "./config";
import {Runtime} from "@pulumi/aws/lambda";


export function tags(prefix: String): aws.Tags {
    return {
        App: "AWS_MVP",
        Env: `${prefix}`
    }
}

export function lambdaTag(prefix: String, type: LambdaLanguageType, runtime?: Runtime): aws.Tags {
    const baseTags = tags(prefix);
    baseTags.Lambda = `${type}`;
    //baseTags.Runtime = `${runtime}`;
    return baseTags;
}
