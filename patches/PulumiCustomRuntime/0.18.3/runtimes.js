"use strict";
// Copyright 2016-2018, Pulumi Corporation.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
Object.defineProperty(exports, "__esModule", { value: true });
exports.DotnetCore1d0Runtime = "dotnetcore1.0";
exports.DotnetCore2d0Runtime = "dotnetcore2.0";
exports.DotnetCore2d1Runtime = "dotnetcore2.1";
exports.Go1dxRuntime = "go1.x";
exports.Java8Runtime = "java8";
exports.NodeJS4d3EdgeRuntime = "nodejs4.3-edge";
exports.NodeJS4d3Runtime = "nodejs4.3";
exports.NodeJS6d10Runtime = "nodejs6.10";
exports.NodeJS8d10Runtime = "nodejs8.10";
exports.NodeJSRuntime = "nodejs";
exports.Python2d7Runtime = "python2.7";
exports.Python3d6Runtime = "python3.6";
exports.CustomRuntime = "provided";
//# sourceMappingURL=runtimes.js.map