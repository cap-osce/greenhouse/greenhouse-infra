/**
 * Runtime is a union type containing all available AWS Lambda runtimes.
 */
export declare type Runtime = "dotnetcore1.0" | "dotnetcore2.0" | "dotnetcore2.1" | "go1.x" | "java8" | "nodejs4.3-edge" | "nodejs4.3" | "nodejs6.10" | "nodejs8.10" | "nodejs" | "python2.7" | "python3.6" | "provided";
export declare let DotnetCore1d0Runtime: Runtime;
export declare let DotnetCore2d0Runtime: Runtime;
export declare let DotnetCore2d1Runtime: Runtime;
export declare let Go1dxRuntime: Runtime;
export declare let Java8Runtime: Runtime;
export declare let NodeJS4d3EdgeRuntime: Runtime;
export declare let NodeJS4d3Runtime: Runtime;
export declare let NodeJS6d10Runtime: Runtime;
export declare let NodeJS8d10Runtime: Runtime;
export declare let NodeJSRuntime: Runtime;
export declare let Python2d7Runtime: Runtime;
export declare let Python3d6Runtime: Runtime;
export declare let CustomRuntime: Runtime;
