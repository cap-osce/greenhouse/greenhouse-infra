#!/usr/bin/env bash
#export pulumiVersion=$(pulumi version)
export awsVersion=$(npm view @pulumi/aws version)
dir="$(dirname $0)";
patchDir="${dir}/PulumiCustomRuntime";

latestPatchVersion="0.18.3";

if [ "$1" == "-f" ]; then
   echo "Patch: Forcing patch $latestPatchVersion to support custom runtimes in @Pulumi/aws $awsVersion"
   cp -v ${patchDir}/${awsVersion}/runtimes.d.ts ${dir}/../node_modules/\@pulumi/aws/lambda/ &&
    cp -v ${patchDir}/${awsVersion}/runtimes.js ${dir}/../node_modules/\@pulumi/aws/lambda/ &&
    echo "DONE: Applied $awsVersion to @Pulumi/aws $awsVersion to support custom runtimes. Good luck..."
 else
    echo "Patch: Applying patch to support custom runtimes in @Pulumi/aws $awsVersion"
    cp -v ${patchDir}/${awsVersion}/runtimes.d.ts ${dir}/../node_modules/\@pulumi/aws/lambda/ &&
    cp -v ${patchDir}/${awsVersion}/runtimes.js ${dir}/../node_modules/\@pulumi/aws/lambda/ &&
    echo "DONE: @Pulumi/aws $awsVersion now supports custom runtimes"
fi

