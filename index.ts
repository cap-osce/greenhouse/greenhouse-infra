import * as pulumi from "@pulumi/pulumi";
import {getStack} from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";
import {tags} from "./tags";
import {GreenhouseConfig} from "./config";
import {SensorEvents} from "./events";
import {rolePolicyAtt, lambdaRole} from "./roles";
import {MicroserviceDescriptor} from "./microservice";
import {eventsTable} from './tables/eventsTable';
import {commandsTable} from './tables/commandsTable';
import {projectionsTable} from './tables/projectionsTable'
import {createLambdaAndMapper, LambdaEventMapperDef} from "./lambdaHelper";

const config = new pulumi.Config("greenhouse-infra");
const prefix = config.require("prefix");
const awsConfig = new pulumi.Config("aws");
const awsRegion = awsConfig.require("region");

const currentCallerIdentity = pulumi.output(aws.getCallerIdentity());

new GreenhouseConfig().run(prefix);

const eventTable = eventsTable(prefix);
const commandTable = commandsTable(prefix);
const projectionTable = projectionsTable(prefix);


//Sensor Projection: NodeJs8 Lambda
const sensorProjectionLambda =new aws.lambda.Function(`${prefix}-sensor-projection-lambda`, {
        runtime: aws.lambda.NodeJS8d10Runtime,
        environment: {
            variables: {
                prefix: `${prefix}`,
                awsRegion: `${awsRegion}`
            },
        },
        code: new pulumi.asset.AssetArchive({
            ".": new pulumi.asset.FileArchive("./target/sensorprojectionlambda.zip"),
        }),
        timeout: 5,
        handler: "sensorprojectionlambda.handler",
        role: lambdaRole.arn,
        tags: tags(prefix)
    },
    {
        dependsOn: [rolePolicyAtt, projectionTable]
    }
);
const sensorProjectionMapping =new aws.lambda.EventSourceMapping(`${prefix}-table-event-sensor-projection-lambda-mapping`, {
        eventSourceArn: eventTable.streamArn,
        enabled: true,
        functionName: sensorProjectionLambda.arn,
        startingPosition: "LATEST",
        batchSize: 5
    },
    {
        dependsOn: [eventTable, sensorProjectionLambda]
    });

//Sensor Projection: Native Lambda - Only works if you patch up the pulumi aws-sdk module. dir patches, execute applyPatches.sh
// const nativeLambdaDef: LambdaEventMapperDef = {
//     prefix: `${prefix}`,
//     awsRegion: awsRegion,
//     lambdaName: `${prefix}-sensor-projection-native-lambda`,
//     runtime: aws.lambda.CustomRuntime,
//     environment: {
//         variables: {
//             prefix: `${prefix}`,
//             awsRegion: `${awsRegion}`
//         }
//     },
//     lambdaPackage: toS3Key("native/sensorsProjection.zip"),
//     lambdaS3Bucket: "greenhouse-lambdas",
//     lambdaHandler: "function.ok",
//     role: lambdaRole,
//     trace: true,
//     eventMapperName: `${prefix}-table-event-sensor-projection-lambda-mapping`,
//     eventSource: eventTable,
//     memorySize: 128,
//     timeout: 10,
//     batchSize: 10
// };
//const lambdaMapper = createLambdaAndMapper(nativeLambdaDef);


let endpoint = new awsx.apigateway.API(`${prefix}-sensor-api`, {
    stageName: "sensor",
    routes: [{
        path: "/{sensorId}",
        method: "GET",
        eventHandler: async (event) => {
            let sensorId = event.pathParameters!["sensorId"];

            const client = new aws.sdk.DynamoDB.DocumentClient();

            const tableData = await client.get({
                TableName: projectionTable.name.get(),
                Key: {Source: sensorId},
                ConsistentRead: true,
            }).promise();


            if (tableData.Item != null) {
                const value = tableData.Item.Value;
                const type = tableData.Item.Type;
                const timestamp = tableData.Item.Timestamp;
                return {
                    statusCode: 200,
                    body: JSON.stringify({sensorId, type, timestamp, value}) + "\n",
                };
            } else {
                return {
                    statusCode: 404,
                    body: "{}\n"
                };
            }
        },
    }],
});

export let awsxAPIEndpoint = endpoint.url;


//TODO simple sensor API

const lambdaSensorAPI = new aws.lambda.Function(`${prefix}-sensor-api-lambda`, {
        runtime: aws.lambda.NodeJS8d10Runtime,
        environment: {
            variables: {
                prefix: `${prefix}`,
                awsRegion: `${awsRegion}`
            },
        },
        code: new pulumi.asset.AssetArchive({
            ".": new pulumi.asset.FileArchive("./target/sensorapi.zip"),
        }),
        timeout: 5,
        handler: "sensorapi.handler",
        role: lambdaRole.arn,
        tags: tags(prefix)
    },
    {
        dependsOn: [lambdaRole]
    });

const sensorRestApi = new aws.apigateway.RestApi(`${prefix}-sensor-restapi`, {},
    {
        dependsOn: []
    });


const resource = new aws.apigateway.Resource(`${prefix}-sensor-resource`, {
        parentId: sensorRestApi.rootResourceId,
        pathPart: "{sensorId}",
        restApi: sensorRestApi
    },
    {
        dependsOn: [sensorRestApi]
    });

const method = new aws.apigateway.Method(`${prefix}-sensor-method`, {
        authorization: "NONE",
        httpMethod: "GET",
        resourceId: resource.id,
        restApi: sensorRestApi,
    },
    {
        dependsOn: [sensorRestApi, resource]
    });

const apigwLambda = new aws.lambda.Permission(`${prefix}-sensor-api-lambda-permission`, {
        action: "lambda:InvokeFunction",
        function: lambdaSensorAPI.arn,
        principal: "apigateway.amazonaws.com",
        sourceArn: pulumi.all([sensorRestApi.id, currentCallerIdentity.accountId, method.httpMethod, resource.path]).apply(([id, accountId, httpMethod, path]) => `arn:aws:execute-api:${awsRegion}:${accountId}:${id}/*/${httpMethod}${path}`),
    },
    {
        dependsOn: [lambdaSensorAPI, sensorRestApi]
    });

const integration = new aws.apigateway.Integration(`${prefix}-sensor-api-integration`, {
        httpMethod: method.httpMethod,
        integrationHttpMethod: "POST",
        resourceId: resource.id,
        restApi: sensorRestApi,
        type: "AWS_PROXY",
        uri: lambdaSensorAPI.arn.apply(arn => `arn:aws:apigateway:${awsRegion}:lambda:path/2015-03-31/functions/${arn}/invocations`),
    },
    {
        dependsOn: [lambdaSensorAPI, sensorRestApi, method, resource, apigwLambda]
    });

const deployment = new aws.apigateway.Deployment(`${prefix}-sensor-deployment`, {
        restApi: sensorRestApi,
    },
    {
        dependsOn: [integration]
    });

const stage = new aws.apigateway.Stage(`${prefix}-sensor-stage`, {
        stageName: "verbose",
        deployment: deployment,
        restApi: sensorRestApi,
        tags: tags(prefix)
    },
    {
        dependsOn: [sensorRestApi, deployment]
    }
);

export let sensorRestAPIEndpoint = pulumi.all([stage.invokeUrl, resource.path]).apply(([baseUrl, path]) => `${baseUrl}${path}`);

//END

const CACTUS_NAME = "cactus";
const SUNFLOWER_NAME = "sunflower";
const GREENHOUSE_NAME = "greenhouse";

const uServices: MicroserviceDescriptor[] = [
    {
        name: CACTUS_NAME,
        acceptEvents: [
            SensorEvents.SENSOR_TEMPERATURE_READING_ACQUIRED,
            SensorEvents.SENSOR_HUMIDITY_READING_ACQUIRED,
            SensorEvents.SENSOR_MOISTURE_READING_ACQUIRED,
            SensorEvents.SENSOR_LUMINOSITY_READING_ACQUIRED
        ],
        generatedEventSuffix: [
            SensorEvents.PLANT_EVENT_SUFFIX_IS_THIRSTY,
            SensorEvents.PLANT_EVENT_SUFFIX_IS_FLOODED,
            SensorEvents.PLANT_EVENT_SUFFIX_IS_COLD,
            SensorEvents.PLANT_EVENT_SUFFIX_IS_HOT,
            SensorEvents.PLANT_EVENT_SUFFIX_AIR_IS_TOO_WET,
            SensorEvents.PLANT_EVENT_SUFFIX_AIR_IS_TOO_DRY,
            SensorEvents.PLANT_EVENT_SUFFIX_IS_DARK,
            SensorEvents.PLANT_EVENT_SUFFIX_IS_BRIGHT
        ],
        lambdaPackage: toLocalFile("eventprocessorcactuslambda.zip"),
        lambdaHandler: "eventprocessorcactuslambda.handler"
    },
    {
        name: SUNFLOWER_NAME,
        acceptEvents: [
            SensorEvents.SENSOR_TEMPERATURE_READING_ACQUIRED,
            SensorEvents.SENSOR_HUMIDITY_READING_ACQUIRED,
            SensorEvents.SENSOR_MOISTURE_READING_ACQUIRED,
            SensorEvents.SENSOR_LUMINOSITY_READING_ACQUIRED
        ],
        generatedEventSuffix: [
            SensorEvents.PLANT_EVENT_SUFFIX_IS_THIRSTY,
            SensorEvents.PLANT_EVENT_SUFFIX_IS_FLOODED,
            SensorEvents.PLANT_EVENT_SUFFIX_IS_COLD,
            SensorEvents.PLANT_EVENT_SUFFIX_IS_HOT,
            SensorEvents.PLANT_EVENT_SUFFIX_AIR_IS_TOO_WET,
            SensorEvents.PLANT_EVENT_SUFFIX_AIR_IS_TOO_DRY,
            SensorEvents.PLANT_EVENT_SUFFIX_IS_DARK,
            SensorEvents.PLANT_EVENT_SUFFIX_IS_BRIGHT
        ],
        lambdaPackage: toLocalFile("eventprocessorsunflowerlambda.zip"),
        lambdaHandler: "eventprocessorsunflowerlambda.handler"
    },
    {
        name: GREENHOUSE_NAME,
        acceptEvents: [
            SensorEvents.SENSOR_TEMPERATURE_READING_ACQUIRED,
            SensorEvents.SENSOR_HUMIDITY_READING_ACQUIRED,
            SensorEvents.SENSOR_MOISTURE_READING_ACQUIRED,
            SensorEvents.SENSOR_LUMINOSITY_READING_ACQUIRED
        ],
        generatedEventSuffix: [
        ],
        lambdaPackage: toLocalFile("eventprocessorgreenhouselambda.zip"),
        lambdaHandler: "eventprocessorgreenhouselambda.handler"
    }
]

const iotReadingLambda = new aws.lambda.Function(`${prefix}-iot-reading-lambda`, {
    runtime: aws.lambda.NodeJS8d10Runtime,
    environment: {
        variables: {
            prefix: `${prefix}`,
            awsRegion: `${awsRegion}`
        },
    },
    code: new pulumi.asset.AssetArchive({
        ".": new pulumi.asset.FileArchive("./target/iotReadingLambda.zip"),
    }),
    timeout: 5,
    handler: "iotreadinglambda.handler",
    role: lambdaRole.arn,
    tags: tags(prefix)
});

const topics = [
    "temperature",
    "humidity",
    "moisture",
    "light"
];

topics.forEach(topic => {
    const rule_name = `${prefix}-rule-${topic}`.replace(/-/g, '_');
    const rule = new aws.iot.TopicRule(rule_name, {
            name: rule_name,
            description: `Rule to consume from ${topic}`,
            enabled: true,
            lambda: {
                functionArn: iotReadingLambda.arn
            },
            sql: `SELECT * FROM 'measurement/${topic}'`,
            sqlVersion: "2015-10-08",
        },
        {
            dependsOn: [eventTable, iotReadingLambda]
        });

    const permission = new aws.lambda.Permission(`allow_iot_rule_${topic}`, {
        action: "lambda:InvokeFunction",
        function: iotReadingLambda.name,
        principal: "iot.amazonaws.com",
        sourceArn: rule.arn,
    });
});

const commandwritingLambda = new aws.lambda.Function(`${prefix}-iot-writing-lambda`, {
    runtime: aws.lambda.NodeJS8d10Runtime,
    environment: {
        variables: {
            prefix: `${prefix}`,
            awsRegion: `${awsRegion}`
        },
    },
    code: new pulumi.asset.AssetArchive({
        ".": new pulumi.asset.FileArchive("./target/commandForwarder.zip"),
    }),
    timeout: 5,
    handler: "commandforwarder.handler",
    role: lambdaRole.arn,
    tags: tags(prefix)
});

const commandTableMapping = new aws.lambda.EventSourceMapping(`${prefix}-iot-writing-lambda-source`, {
    eventSourceArn: commandTable.streamArn,
    functionName: commandwritingLambda.arn,
    startingPosition: "LATEST",
});

uServices.forEach((m: MicroserviceDescriptor) => {

    //create projection table
    const microserviceProjectionTable = new aws.dynamodb.Table(
        `${prefix}-${m.name}-projection`,
        {
            name: `${prefix}-${m.name}-projection`,
            attributes: [
                {name: "Type", type: "S"},
            ],
            billingMode: "PROVISIONED",
            hashKey: "Type",
            streamEnabled: false,
            readCapacity: 5,
            writeCapacity: 5,
            tags: tags(prefix)
        }
    );

    const uServiceLambdaDef: LambdaEventMapperDef = {
        prefix: `${prefix}`,
        awsRegion: awsRegion,
        lambdaName: `${prefix}-${m.name}-eventprocessor-nodejs-lambda`,
        runtime: aws.lambda.NodeJS8d10Runtime,
        environment: {
            variables: {
                serviceName: `${m.name}`,
                prefix: `${prefix}`,
                awsRegion: `${awsRegion}`
            }
        },
        lambdaPackage: `${m.lambdaPackage}`,
        lambdaHandler: `${m.lambdaHandler}`,
        role: lambdaRole,
        trace: true,
        eventMapperName: `${prefix}-table-event-${m.name}-eventprocessor-lambda-mapping`,
        eventSource: eventTable,
        memorySize: 128,
        timeout: 10,
        batchSize: 10
    };

    const uServiceLambda = createLambdaAndMapper(uServiceLambdaDef, [rolePolicyAtt]);

});

function toLocalFile(url: string) {
    return `target/${url}`;
}

function toS3Key(url: string) {
    //   lambdaPackage: "snapshot/greenhouse-joao/native/sensorsProjection.zip",
    if (isProd()){
        return `release/${prefix}/${url}`;
    }else
    {
        return `snapshot/${prefix}/${url}`;
    }
}

function isProd(): boolean {
    if (getStack() === "prod") {
        return true;
    } else {
        return false;
    }
}

function isDev(): boolean {
    return !isProd();
}
