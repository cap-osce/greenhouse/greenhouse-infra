import {lambdaRole, rolePolicyAtt} from "./roles";
import {tags} from "./tags";
import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";



const config = new pulumi.Config("greenhouse-infra");
const prefix = config.require("prefix");
const awsConfig = new pulumi.Config("aws");
const awsRegion = awsConfig.require("region");


//Sensor Projection: NodeJs8 Lambda
// const sensorProjectionLambda =new aws.lambda.Function(`${prefix}-sensor-projection-lambda`, {
//         runtime: aws.lambda.NodeJS8d10Runtime,
//         environment: {
//             variables: {
//                 prefix: `${prefix}`,
//                 awsRegion: `${awsRegion}`
//             },
//         },
//         code: new pulumi.asset.AssetArchive({
//             ".": new pulumi.asset.FileArchive("./target/sensorprojectionlambda.zip"),
//         }),
//         timeout: 5,
//         handler: "sensorprojectionlambda.handler",
//         role: lambdaRole.arn,
//         tags: tags(prefix)
//     },
//     {
//         dependsOn: [rolePolicyAtt, projectionTable]
//     }
// );
// const sensorProjectionMapping =new aws.lambda.EventSourceMapping(`${prefix}-table-event-sensor-projection-lambda-mapping`, {
//         eventSourceArn: eventTable.streamArn,
//         enabled: true,
//         functionName: sensorProjectionLambda.arn,
//         startingPosition: "LATEST",
//         batchSize: 5
//     },
//     {
//         dependsOn: [eventTable, sensorProjectionLambda]
//     });


//Sensor Projection: JAVA LAMBDA
// const sensorProjectionLambda =
// new aws.lambda.Function(`${prefix}-sensor-projection-java-lambda`, {
//         runtime: aws.lambda.Java8Runtime,
//         environment: {
//             variables: {
//                 prefix: `${prefix}`,
//                 awsRegion: `${awsRegion}`
//             },
//         },
//         code: new pulumi.asset.AssetArchive({
//             ".": new pulumi.asset.FileArchive("./target/sensors-projection-1.0-SNAPSHOT.zip"),
//         }),
//         timeout: 60,
//         memorySize: 256,
//         handler: "uk.capgemini.sensors.lambda.UpdateProjection::handleRequest",
//         role: lambdaRole.arn,
//         tags: tags(prefix)
//     },
//     {
//         dependsOn: [rolePolicyAtt, projectionTable]
//     }
// );
// const sensorProjectionMapping = new aws.lambda.EventSourceMapping(`${prefix}-table-event-sensor-projection-lambda-mapping`, {
//         eventSourceArn: eventTable.streamArn,
//         enabled: true,
//         functionName: sensorProjectionLambda.arn,
//         startingPosition: "LATEST",
//         batchSize: 5
//     },
//     {
//         dependsOn: [eventTable, sensorProjectionLambda]
//     });