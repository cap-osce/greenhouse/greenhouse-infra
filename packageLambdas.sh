#!/usr/bin/env bash

echo "packing lambdas in greenhouse-infra"
rm -rf target
mkdir -p target
zip -j target/eventprocessorlambda.zip lambdas/eventprocessorlambda.js lambdas/events.js lambdas/physical.js
zip -j target/eventprocessorgreenhouselambda.zip lambdas/eventprocessorgreenhouselambda.js lambdas/events.js lambdas/physical.js
zip -j target/eventprocessorsunflowerlambda.zip lambdas/eventprocessorsunflowerlambda.js lambdas/events.js lambdas/physical.js
zip -j target/eventprocessorcactuslambda.zip lambdas/eventprocessorcactuslambda.js lambdas/events.js lambdas/physical.js
zip -j target/sensorprojectionlambda.zip lambdas/sensorprojectionlambda.js lambdas/events.js lambdas/physical.js
zip -j target/selectorlambda.zip lambdas/selectorlambda.js lambdas/events.js lambdas/physical.js
zip -j target/iotReadingLambda.zip lambdas/iotreadinglambda.js
zip -j target/commandForwarder.zip lambdas/commandforwarder.js
zip -j target/sensorapi.zip lambdas/sensorapi.js lambdas/events.js lambdas/physical.js
