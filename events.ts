import {AttributeValue} from "aws-sdk/clients/dynamodb";


export const SensorEvents = {
    SENSOR_TEMPERATURE_READING_ACQUIRED: "sensor.temperature.reading-acquired",
    SENSOR_HUMIDITY_READING_ACQUIRED: "sensor.humidity.reading-acquired",
    SENSOR_MOISTURE_READING_ACQUIRED: "sensor.moisture.reading-acquired",
    SENSOR_LUMINOSITY_READING_ACQUIRED: "sensor.luminosity.reading-acquired",

    PLANT_EVENT_SUFFIX_IS_THIRSTY: "plant-is-thirsty",
    PLANT_EVENT_SUFFIX_IS_FLOODED: "plant-is-flooded",
    PLANT_EVENT_SUFFIX_IS_COLD: "plant-is-cold",
    PLANT_EVENT_SUFFIX_IS_HOT: "plant-is-hot",
    PLANT_EVENT_SUFFIX_AIR_IS_TOO_WET: "air-is-too-wet",
    PLANT_EVENT_SUFFIX_AIR_IS_TOO_DRY: "air-is-too-dry",
    PLANT_EVENT_SUFFIX_IS_DARK: "plant-is-dark",
    PLANT_EVENT_SUFFIX_IS_BRIGHT: "plant-is-bright"
}

export const AllSensorEvents = {

    all: [
        SensorEvents.SENSOR_TEMPERATURE_READING_ACQUIRED,
        SensorEvents.SENSOR_HUMIDITY_READING_ACQUIRED,
        SensorEvents.SENSOR_MOISTURE_READING_ACQUIRED,
        SensorEvents.SENSOR_LUMINOSITY_READING_ACQUIRED
    ]
}

export const AllPlantEvents = {

    all: [
        SensorEvents.PLANT_EVENT_SUFFIX_IS_THIRSTY,
        SensorEvents.PLANT_EVENT_SUFFIX_IS_FLOODED,
        SensorEvents.PLANT_EVENT_SUFFIX_IS_COLD,
        SensorEvents.PLANT_EVENT_SUFFIX_IS_HOT,
        SensorEvents.PLANT_EVENT_SUFFIX_AIR_IS_TOO_WET,
        SensorEvents.PLANT_EVENT_SUFFIX_AIR_IS_TOO_DRY,
        SensorEvents.PLANT_EVENT_SUFFIX_IS_DARK,
        SensorEvents.PLANT_EVENT_SUFFIX_IS_BRIGHT
    ]
}

export class DynamoTableEvent {
    Source: AttributeValue
    Type: AttributeValue
    Value: AttributeValue
}