import * as aws from "@pulumi/aws";
import { tags } from "../tags";

export function projectionsTable(prefix: string) {
    return new aws.dynamodb.Table(
        `${prefix}-sensor-projection`,
        {
            name: `${prefix}-sensor-projection`,
            attributes: [
                { name: "Source", type: "S" }
            ],
            billingMode: "PROVISIONED",
            hashKey: "Source",
            streamEnabled: false,
            readCapacity: 5,
            writeCapacity: 5,
            tags: tags(prefix)
        }
    );
}