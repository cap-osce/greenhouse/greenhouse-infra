import * as aws from "@pulumi/aws";
import {tags} from "../tags";

export function eventsTable(prefix: string) {
    return new aws.dynamodb.Table(
        `${prefix}-events`,
        {
            name: `${prefix}-events`,
            attributes: [
                {name: "Timestamp", type: "N"},
                {name: "Type", type: "S"},
                {name: "Source", type: "S"},
                {name: "Value", type: "N"}
            ],
            billingMode: "PROVISIONED",
            hashKey: "Source",
            rangeKey: "Timestamp",
            localSecondaryIndexes: [{
                name: "SourceByValue",
                rangeKey: "Value",
                nonKeyAttributes: ["Type"],
                projectionType: "INCLUDE"
            }, {
                name: "SourceByType",
                rangeKey: "Type",
                nonKeyAttributes: ["Value"],
                projectionType: "INCLUDE"
            }],
            streamViewType: "NEW_IMAGE",
            streamEnabled: true,
            readCapacity: 5,
            writeCapacity: 5,
            tags: tags(prefix)
        }
    );
}