import * as aws from "@pulumi/aws";
import {tags} from "./tags";
import * as pulumi from "@pulumi/pulumi";

const config = new pulumi.Config("greenhouse-infra");
const prefix = config.require("prefix");


let policy: aws.iam.PolicyDocument = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "lambda.amazonaws.com",
            },
            "Effect": "Allow",
            "Sid": "",
        },
    ]
};

export const lambdaRole = new aws.iam.Role(`${prefix}-lambda-role`, {
    assumeRolePolicy: JSON.stringify(policy),
    tags: tags(prefix)
});

export const lambdaPolicy = new aws.iam.RolePolicy(`${prefix}-lambda-policy`, {
    policy: `{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
         "dynamodb:*",
         "lambda:*",
         "kinesis:*",
         "cloudwatch:*",
         "log:*",
         "iot:Publish",
         "sns:*",
         "sqs:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
`,
    role: lambdaRole.id
},{
    dependsOn: [lambdaRole]
});


export const rolePolicyAtt = new aws.iam.RolePolicyAttachment(`${prefix}-lambda-policy-attachment`, {
    role: lambdaRole,
    policyArn: aws.iam.AWSLambdaFullAccess
}, {
    dependsOn: [lambdaPolicy]
});